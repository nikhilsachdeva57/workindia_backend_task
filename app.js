//dependencies
var express = require('express');
var app = express();
var passwordHash = require('password-hash');
const mysql = require('mysql');

const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());

//mysql connection
const connection = mysql.createConnection({
		host     : 'localhost',
		user     : 'root',
		password : 'nikhil99',
		database : 'workindia'
})
connection.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
  });


var server = app.listen(process.env.PORT || 8080, function () {
   var port = server.address().port;
   console.log("App now running on port", port);
});




//register user
app.post('/app/agent', (req,res) => {
    //req = JSON.parse(req.body)
	const id = req.body.agent_id
    const pass = passwordHash.generate(req.body.password)
    const query = "insert into users (agent_id,password) values (?,?);"
    console.log(req.body)
	connection.query(query,[id,pass],(err, results, fields) => {
		if(err){
			console.log("Error")
            console.log(err.message)
            res.json(err.message)
			res.sendStatus(500)
			return
		}
		
        result = {status : "account created" , status_code : 200}
        res.json(result)
		res.end()


	})
})

//authenticate user
app.post('/app/agent/auth', (req,res) => {
	const id = req.body.agent_id
    const pass = req.body.password
    const query = "select * FROM users where agent_id = " + id;
	connection.query(query,[id,pass],(err, results, fields) => {
		if(err){
			console.log("Error")
            console.log(err.message)
            res.json(err.message)
			res.sendStatus(500)
			return
		}
		if(results[0] == null || !passwordHash.verify(pass,results[0].password)){
            result = {status : "failure" , status_code : 401}
            res.json(result)
            res.end()
        }
        else{
            result = {status : "success" , agent_id : id, status_code : 200}
            res.json(result)
            res.end()
        }

	})
})




//insert post
app.post('/app/sites', (req,res) => {
	const title = req.body.title
    const desc = req.body.description
    const category = req.body.category
    const due_date = req.body.due_date
	const query = "insert into posts (title,description, category,due_date ) values (?,?, ?, ?);"
	connection.query(query,[title,desc, category, due_date],(err, results, fields) => {
		if(err){
			console.log("Error")
			console.log(err.message)
			res.sendStatus(500)
			return
		}
		
		console.log(results)
        result = {status : "success" , status_code : 200}
        res.json(result)
		res.end()


	})
})
//get posts
app.get('/app/sites/list',(req,res) => {
   
	connection.query("SELECT * FROM posts order by due_date desc",(err,rows,query) =>{
        res.json(rows)
        
	})
})